TexWeather
==========
©2024 by Hume2, license CC0

An exhaustive set of weather icons suitable for scientific papers (1152 icons in total).

See weather_example.pdf for more details.

Citation:
```
@misc{texweather,
    author   = {Hume2},
    title    = {TexWeather},
    howpublished = {\url{https://gitlab.com/Hume2/texweather}},
    year = {2024},
    month = {04},
}
```
